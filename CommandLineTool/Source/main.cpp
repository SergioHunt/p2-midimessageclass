//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include <cmath>

class MidiMessage
{
public:
    MidiMessage() //Constructor
    {
      number = 60;
      std::cout << "Constructor\n";
    }
    ~MidiMessage() //Destructor
    {
       std::cout << "Destructor\n";
    }
    void setNoteNumber (int value) //Mutator
    {
        number = value;
        
    }
    int getNoteNumber() const //Acessor
    {
        return number;
    }
    float getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
private:
    int number;
    
};

MidiMessage note;

int main()
{
    std::cout << "Practical 2\n";
    
    
    int printValue = note.getNoteNumber();
    float frequency = note.getMidiNoteInHertz();
    int input;
    
    std::cout << "Note Number \n" << printValue << std::endl;
    std::cout << "Frequency  \n" << frequency << std::endl;
    
    std::cout << "Enter New Note Value\n";
    std::cin >> input;
    note.setNoteNumber(input);
    printValue = note.getNoteNumber();
    std::cout << "New Note Number \n" << printValue << std::endl;
    frequency = note.getMidiNoteInHertz();
    std::cout << "New Frequency \n" << frequency << std::endl;
    
    return 0;
}
